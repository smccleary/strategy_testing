import backtrader as bt


class BasicSMA(bt.Strategy):
    params = (
        ('maperiod_min', 5),
        ('maperiod_max', 20),
    )

    def log(self, txt, dt=None):
        """ Logging function fot this strategy """
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))

    def difference(self, a, b):
        if a <= b:
            return b - a
        else:
            return (a - b) * 2

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.bar_executed = len(self)
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None

        # Add a MovingAverageSimple indicator
        self.sma_short = bt.indicators.SimpleMovingAverage(self.datas[0], period=self.params.maperiod_min)
        self.sma_long = bt.indicators.SimpleMovingAverage(self.datas[0], period=self.params.maperiod_max)

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None

    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        self.log('|--- OPERATION PROFIT, GROSS %.2f, NET %.2f' %
                 (trade.pnl, trade.pnlcomm))

    def next(self):
        closing_price = self.dataclose[0]
        sma_short_now = self.sma_short[0]
        sma_long_now = self.sma_long[0]

        if self.order:
            return

        # Check if we are in the market
        if not self.position:
            if closing_price > sma_long_now:
                self.order = self.buy()

            elif (self.sma_short[-1] < sma_long_now) and (sma_short_now >= sma_long_now):
                self.order = self.buy()

        else:
            if closing_price < sma_long_now:
                self.order = self.sell()

            elif (self.sma_short[-1] > sma_long_now) and (sma_short_now < sma_long_now):
                self.order = self.sell()
